User name = alonhrl  
Python environment = predictive  
Jupyter kernel = python_spark  
Project folder = PredictiveMaintenance 

## HOW TO RUN NOTEBOOKS ON A LOCAL MACHINE 
## Installation  
1) Install Anaconda 3  
`curl -O https://repo.continuum.io/archive/Anaconda3-5.3.1-Linux-x86_64.sh` (or download Anaconda from the browser)  
`bash Anaconda3-5.3.1-Linux-x86_64.sh` (Answer YES to all questions including the question about Visual Studio Code installation )  
`export PATH=/home/alonhrl/anaconda3/bin:$PATH`  
2) Create Python environment  
`conda create --name predictive python=3.7`  
3) Activate environment  
`source activate predictive` 
4) Install Azure ML SDK  
`pip install azureml-defaults` 
5) Install libraries  
`conda install pandas`   
`conda install matplotlib`  
`pip install ggplot`  
6) Correct ggplot:  
In file `/home/alonhrl/anaconda3/envs/predictive/lib/python3.7/site-packages/ggplot/stats/smoothers.py`  
Change line 4 from `from pandas.lib import Timestamp` to `from pandas import Timestamp`  
Change line 14 from `pd.tslib.Timestamp` to `pd.Timestamp` 
7) Install Spark  
Check if Java is installed `java -version`  
Install Java if it is not installed `sudo apt install default-jre`  
Check if Scala is installed `scala -version`  
Install Scala if it is not installed `sudo apt install scala`  
Download Spark `curl -O http://mirror.linux-ia64.org/apache/spark/spark-2.4.0/spark-2.4.0-bin-hadoop2.7.tgz`  
Unpack Spark `tar -zxvf spark-2.4.0-bin-hadoop2.7.tgz`  
Move Spark `sudo mv spark-2.4.0-bin-hadoop2.7 /usr/local/spark`  
Add the line `export PATH=/usr/local/spark/bin:$PATH` to `.bashrc` file in `/home/alonhrl` folder and run `source .bashrc`  
Check if Spark is accessible with `spark-shell`  
8) Install pyspark  
`conda install pyspark`  
9) Create new IPython kernelspec for Jupter  
`conda install ipykernel`  
`python -m ipykernel install --user --name python_spark`  
( As alternative we can just create new folder for kernelspec, for example, 
`mkdir /usr/local/share/jupyter/kernels/python_spark`, then copy `kernel.json` from this repository to the folder )    
10) List existing kernelspecs  
`jupyter kernelspec list` 
It should contain path to `python_spark` kernelspec file, for example,  
`/home/alonhrl/.local/share/jupyter/kernels/python_spark`  
Open file `kernel.json` in this folder and check if it links to `predictive` environment. It should contain something like this   
`"/home/alonhrl/anaconda3/envs/predictive/bin/python"`  
11) Install GIT  
`sudo apt install git`  
12) Add Spark system variables to kernelspec file (optional) as defined in `kernel.json` file  
13) Add `export SPARK_HOME=/usr/local/spark` to `.bashrc` file in `/home/alonhrl`  
14) Add `export PYSPARK_PYTHON=/home/ubuntu/anaconda3/bin/python` to `.bashrc` file in `/home/alonhrl` 
15) Run `source .bashrc` 
16) Find `/usr/local/spark/conf/spark-deafaults.conf` and add (or edit) line `spark.driver.memory 10g` (or at least 5g). If there is no `/usr/local/spark/conf/spark-deafaults.conf` file, find `spark-defaults.conf.template` file, uncomment and edit corresponding line (`#spark.driver.memory 10g`), write file as `spark-defaults.conf`   
17) Edit `Code/blob_parameters.py` file to define account name and account key for blob   

## Running notebooks 
To start Jupyter notebook:  
1) `cd /home/alonhrl/PredictiveMaintenance`  
2) `jupyter notebook`  
3) Open `Code` folder 
4) Select notebook  
5) Select `python_spark` kernel 

## Prerequisites: 
 - `azureml-defaults` 
 - Python >= 3.6 
 - Spark 
 - `pyspark` 
 - `numpy`  
 - `pandas` 
 - `matplotlib` 
 - `ggplot` 

## INSTALLATION TO RUN SCRIPTS ON A LOCAL MACHINE 
1) Install Anaconda 3  
`curl -O https://repo.continuum.io/archive/Anaconda3-5.3.1-Linux-x86_64.sh` (or download Anaconda from the browser)  
`bash Anaconda3-5.0.1-Linux-x86_64.sh` (Answer YES to all questions or customize folder where Anaconda will be installed)  
`export PATH=/home/alonhrl/anaconda3/bin:$PATH` 
2) Create Python environment  
`conda create --name predictive python=3.7`  
3) Activate environment  
`source activate predictive` 
4) Install Azure ML SDK  
`pip install azureml-defaults` 
5) Install libraries  
`conda install pandas`   
6) Install Spark  
Check if Java is installed `java -version`  
Install Java if it is not installed `sudo apt install default-jre`  
Check if Scala is installed `scala -version`  
Install Scala if it is not installed `sudo apt install scala`  
Download `spark-2.4.0-bin-hadoop2.7.tgz`  
Unpack Spark `tar -zxvf spark-2.4.0-bin-hadoop2.7.tgz`  
Move Spark `sudo mv spark-2.4.0-bin-hadoop2.7 /usr/local/spark`  
Add the line `export PATH=/usr/local/spark/bin:$PATH` to `.bashrc` file in `/home/alonhrl` folder and run `source .bashrc`  
Check if Spark is accessible with `spark-shell`  
7) Install pyspark  
`conda install pyspark`  
8) Install GIT  
`sudo apt install git`  

Prerequisites: 
 - `azureml-defaults` 
 - Python >= 3.6 
 - Spark 
 - `pyspark` 
 - `numpy` 
 - `pandas` 


## RUNNING CODE 
There are two ways to run Predictive Maintenance code: 
1) From notebook 
2) Via job submission 

In any case, we will need to create Azure ML Workspace where all Experiments, Runs, Logs, Models, Computes etc will be stored. 
Workspace can be created from Azure Dashboard, with use of Python SDK, using ML extension to Azure CLI or using Azure AI extention to Visual Studio Code. 

`PrepareWorkspace.ipynb` notebook creates new Azure ML Workspace within new resource group. It also writes workspace configuration, 
`aml_config\PredictiveMaintenanceWSConfig.json`, file which wil then be used for loading workspace object into Python code. 
This workspace will be used for both running the `Predictive Maintenance` notebooks from the `Code` folder as well as for 
submitting jobs to different Compute Targets. 


## Running Notebooks  
In this case, the notebook will be running locally or on remote server, for example, Azure Data Science Virtual Machine. 
Each notebook will be considered as a separate experiment. Each run of a notebook will create new Run of the corresponding Experiment.  
All needed information will be logged into workspace. 

To run Predictive Maintenance notebooks we need to have Python >= 3.6 as well as Spark installed 
with corresponding environmrnt variables. Jupyter kernel should be edited accordingly. For example, 
on Azure Data Science VM `SPARK 3 PYTHON LOCAL` kernel may be used. Corresponding kernel configuration file, 
`/usr/local/share/jupyter/kernels/spark-3-python`, should be edited to reference to Python 3.6 or newer. 

To start Jupyter: 
1) `cd /home/alonhrl/PredictiveMaintenance`  
2) `jupyter notebook`  
For each notebook select `python_spark` kernel 

To select `read_from_blob` and `write_to_blob` parameters find them in 
1) Cell No 8 in the `1_data_ingestion` notebook 
2) Cell No 7 in the `2_feature_engineering` notebook 
3) Cell No 7 in the `3_model_building` notebook 

## Running Scripts 
To run `Data Ingestion` script: 
1) Activate environment: `source activate predictive` 
2) Enter `predictivemaintenance\Code` folder: `cd Code` 
3) Run script: `python 1_data_ingestion.py write_to_blob` if writing Parquet files to blob is needed 
4) Run script: `python 1_data_ingestion.py` if Parquet files are to be written locally  

To run `Feature Engineering` script: 
1) Activate environment: `source activate predictive` 
2) Enter `predictivemaintenance\Code` folder: `cd Code` 
3) Run script: `python 2_feature_engineering.py read_from_blob write to blob` if reading and writing Parquet files from/to blob is requested 
4) Run script: `python 2_feature_engineering.py read_from_blob` if reading Parquet files from blob is requested, Parquet file containing fetures will be written locally   
5) Run script: `python 2_feature_engineering.py write to blob` if writing Parquet file containing features to blob is requested, local Parquet files containing data needed to create features will be used 
6) Run script: `python 2_feature_engineering.py` if local Parquet files are used   

To run `Train` script: 
1) Activate environment: `source activate predictive` 
2) Enter `predictivemaintenance\Code` folder: `cd Code` 
3) Run script: `python train.py read_from_blob` if reading Parquet files from blob is requested  
4) Run script: `python train.py` if local Parquet files are used   

## Submitting training job via script 
Job can be submitted to different Compute Targets: 
 - Local machine (it is also possible to use Azure VM as a local machine) 
 - Remote server, for example, Azure VM 
 - Auto created AML compute which will be created for each Run 
 - Predefined cluster which can scale from 0 to predefined number of nodes 

 For each of this configurations corresponding Run Configuration file is created: 
 - `local.runconfig`  
 - `amlcompute.runconfig`  
 - `cluster.runconfig` 

Training model script is in `train.py` file. To run job on a Compute Target `Submit_Run.ipynb` notebook or `Submit_Run.py` script can be used. 

To start job: 
1) `cd /home/alonhrl/PredictiveMaintenance` 
2) `source activate predictive` 
3) Optionally: `code .` to start Visual Studio Code, then `Ctrl-Shift-P`, then start typing and click `Python: Select Interpreter`, select `predictive` environment 
4) Edit `Submit_Run.py` to select Compute Target: local, AMLCompute, predefined cluster 
5) Run `Code/Submit_Run.py` 
